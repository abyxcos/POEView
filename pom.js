// TODO: allow URLs to bookmark accounts/characters
//       check query parameters and set the variables if it's not null
//       kick off events on load for each non-null field

// TODO: Document the character/account/item APIs


// Global configuration
var proxy_server = 'https://poeview.com:8080';
var debug = 0;
// Generate a 2D skeleton to map the items onto
var skeleton = ['Weapon', 'Offhand', 'Helm', 'Amulet', 'BodyArmour', 'Ring', 'Ring2', 'Belt', 'Gloves', 'Boots', 'Weapon2', 'Offhand2'];
// TODO: save the url for linking

// Set some sane AJAX defaults
$.ajaxSettings.type = 'GET';
$.ajaxSettings.dataType = 'json';

// TODO: Check for errors
// TODO: If we get a permissions error, we should return a link to the permissions page for that account
$(function () {
	// Get all running leagues
	// Reverse the order to weigh HC, SSF, league greater than standard SC
	// In most cases, we want the character from the hardest and freshest league
	function getRunningLeagues (context) {
		$.ajax({
			url: `${proxy_server}/leagues?type=main&compact=1`,
			context: context,
			success: function (data) {
				this.running_leagues = _.pluck(data, 'id').reverse();
			},
		});
	}

	// Fetch all the relevant data from this account
	// Group the characters by their (current) league
	// Pluck the leagues this account participates in and trim them
	// We trigger a refresh of the league picker when we're done
	// TODO: Character picker depends on the league filter, not us
	function getAccountData (context, account_name) {
		$.ajax({
			url: `${proxy_server}/characters?accountName=${account_name}`,
			context: context,
			success: function (data) {
				this.character_list = _.groupBy(data, 'league');

				this.league_list = _.pluck(data, 'league')
				this.league_list = _.intersection(this.running_leagues, this.league_list);

				// Update the league list
				this.league_list.map(function (league) {
					this.leaguePicker.append($(`<option>${league}</option>`));
				}, this);

				this.leaguePicker.trigger("change");
				this.leaguePicker.show();
			},
		});
	};

	// Fetch the actual character data
	function getCharacterData (context, account_name, character_name) {
		$.ajax({
			url: `${proxy_server}/characters/get-items?accountName=${account_name}&character=${character_name}`,
			context: context,
			success: function (data) {
				// We need to append to an NWT object still
				var legacyCharacterBox = n.one('#gear');

				skeleton.map(function (slot) {
					var item = _.findWhere(data.items, { inventoryId: slot });
					if (!item) { return; }
					if (debug) { console.log(item); }

					var {el, abyssal} = parseItem(item);
					el.appendTo(legacyCharacterBox);

					if (abyssal) {
						abyssal.forEach(function (jewel) {
							// Abyssal jewels can't nest
							var {el, _unused} = parseItem(jewel);
							el.appendTo(legacyCharacterBox);
						});
					}
				}, this);

				// Do the flasks now
				var flasks = _.where(data.items, { inventoryId: "Flask" });
				_.sortBy(flasks, "x").map(function (flask) {
					var {el, _unused} = parseItem(flask);
					el.appendTo(legacyCharacterBox);
				});
			},
		});
	};

	var AppView = Backbone.View.extend({
		el: $("#poeview"),

		events: {
			"submit"					: "onAccountChangeHandler",
			"change #league"		: "onLeagueChangeHandler",
			"change #character"	: "onCharacterChangeHandler",
		},

		initialize: function () {
			getRunningLeagues(this);

			// Character search features
			this.accountPicker = this.$("#account");
			this.leaguePicker = this.$("#league");
			this.characterPicker = this.$("#character");

			// Character data features
			this.characterBox = this.$("#gear");
		},

		render: function () {
		},

		// When we get a new account search we want to fetch league and character data
		// Update the list of participating leagues then fire a change event
		onAccountChangeHandler: function (e) {
			e.preventDefault();
			this.account_name = this.accountPicker.val().trim();

			getAccountData(this, this.account_name);
		},

		// League filter was changed, update the character view to reflect that
		onLeagueChangeHandler: function () {
			this.characterPicker.empty();
			_.sortBy(this.character_list[this.leaguePicker.val()], "experience").reverse().map(function (character) {
				this.characterPicker.append($(
					`<option>[${character.level}] ${character.name} (${character.class})</option>`,
					{ value: character.name }
				));
			}, this);

			this.characterPicker.trigger("change");
			this.characterPicker.show();
		},

		// A character was selected, render the new one
		onCharacterChangeHandler: function () {
			var gearList = [];

			this.characterBox.empty();
			getCharacterData(this, this.account_name, this.characterPicker.val());
		},
	});

	var App = new AppView;
});

/************/
/* Populate */
/************/

// Populate the gear list
// TODO: Dynamically add abyssal jewels to the list somehow
//       item.socketedItems[N].category (jewels: abyss)
function gear_list (o) {
	var gearList = [];
	// Clear out the old gear set
	n.one('#gear').anim({opacity:0}, 0.3).wait(1).remove();
	var elGear = n.node.create('<div id="gear"></div>').setStyle('opacity', 0);

	// Sort our gear list by skeletal order and create a queue to iterate over
	skeleton.forEach(function (slot) {
		// Search for an item by slot
		var item = o.obj.items.filter(function (item) {
			return item.inventoryId === slot;
		}).shift();

		// If an item is unequipped, skip it
		// TODO: Do we want a placeholder for the slot?
		if (!item) { return; }

		// Print out items for debug
		if (debug) { console.log(item); }

		gearList.push(item);
	});

	// Iterate over our list of found gear
	//gearList.forEach(function (item) {
	while (gearList) {
		var item = gearList.shift();
		if (!item) { break; }

		var {el, abyssal} = parseItem(item);

		// TODO: Look for any jewels in an item
		// TODO: Push jewels onto the item list so the next iteration lists them?
		if (abyssal) {
			abyssal.forEach(function (jewel) {
				gearList.unshift(jewel);
			});
		}

		// TODO: Flasks?

		el.appendTo(elGear);
	}
	//});

	n.one('body').append(elGear);
}

/*********************/
/* Utility functions */
/*********************/

function parseItem (item) {
	var abyssal = [];

	// Create box to hold an item
	// TODO: Add an anchor to the body part name for a quick link
	//var el = n.node.create(tag`<div id="${item.invetoryId}" class="item"></div>`);
	var el = n.node.create(`<div class="item"></div>`);
	var elName = n.node.create('<div class="name"></div>').appendTo(el);
	var elProperties = n.node.create('<div class="properties"></div>').appendTo(el);
	var elImplicits = n.node.create('<div class="implicits"></div>').appendTo(el);
	var elExplicits = n.node.create('<div class="explicits"></div>').appendTo(el);
	var elAttributes = n.node.create('<div class="attributes"></div>').appendTo(el);

	// Insert the icon above the item for now
	// TODO: Make a box to contain the icon even if the item has no stats (tabula rasa)
	if (item.icon) {
		elProperties.prepend(n.node.create(`<p><img src="${item.icon}"/></p>`).addClass('icon'));
	}

	// Set an item rarity
	switch (item.frameType) {
		case 0:
			el.addClass('normal');
			break;
		case 1:
			el.addClass('magic');
			break;
		case 2:
			el.addClass('rare');
			break;
		case 3:
			el.addClass('unique');
			break;
	}

	// Slice name because items come in as:
	// "<<set:MS>><<set:M>><<set:S>>Piscator's Vigil"
	// Blue and white items come in with only a type
	n.node.create(`<p>${item.name.replace(/<<.*>>/, '')}</p>`).appendTo(elName);
	n.node.create(`<p>${item.typeLine.replace(/<<.*>>/, '')} (i${item.ilvl})</p>`).appendTo(elName);

	// Handle innate properties (armor value, dps, etc)
	// May be null for jewelry
	if (item.properties) {
		item.properties.forEach(function (property) {
			if(property.values && property.values[0]) {
				if (property.displayMode == 0) {
					var elProp = n.node.create(`<p>${property.name}: </p>`);

					property.values.forEach(function (value) {
						var augment;
	
						// If we have an augment, map it to a real name
						switch (value[1]) {
							case 0:	// no augments
								break;
							case 1:	// simple magic augmented property
								augment = 'magic'; break;
							case 4:	// fire
								augment = 'fire'; break;
							case 5:	// cold
								augment = 'cold'; break;
							case 6:	// lightning
								augment = 'lightning'; break;
							case 7:	// chaos
								augment = 'chaos'; break;
							default:
								// XXX: 2+3 are missing, if we see these or new ones, say something
								console.log(`${item.name} has an unknown property of ${value}`);
						}
						n.node.create(`<span>${value[0]}</span>`).addClass(augment).appendTo(elProp);
					});
				} else if (property.displayMode == 3) {
					var propertyLine = property.name.replace(/%(\d*)/g, function (match, p1, offset, string) {
						var stringBuilder = `${property.values[0][p1]}`;
						//if (property.values[p1[1]] === 1) { stringBuilder.addClass(augment); }
						return stringBuilder;
					});

					var elProp = n.node.create(`<p>${propertyLine}</p>`);
				}

				elProp.appendTo(elProperties);
			}
		});
	}

	// Handle implicits
	// Implicits, (double) corruptions, lab enchants
	if (item.implicitMods) {
		item.implicitMods.forEach(function (implicit) {
			n.node.create(`<p>${implicit}</p>`).appendTo(elImplicits);
		});
	}
	if (item.enchantMods) {
		n.node.create(`<p>${item.enchantMods}</p>`).addClass('enchanted').appendTo(elImplicits);
	}

	// Handle explicits
	// Check for a zero mod item just because?
	// Crafted mods always go at the bottom
	if (item.explicitMods) {
		item.explicitMods.forEach(function (mod) {
			n.node.create(`<p>${mod}</p>`).appendTo(elExplicits);
		});
	}
	if (item.craftedMods) {
		item.craftedMods.forEach(function (mod) {
			n.node.create(`<p>${mod}</p>`).addClass('crafted').appendTo(elExplicits);
		});
	}

	// Handle corrupted/Shaper/Elder items. Corrupted applies last
	if (item.shaper) {
		n.node.create(`<p>Shaper Item</p>`).appendTo(elAttributes);
		el.addClass('shaper');
	}
	if (item.elder) {
		n.node.create(`<p>Elder Item</p>`).appendTo(elAttributes);
		el.addClass('elder');
	}
	if (item.corrupted) {
		n.node.create(`<p>Corrupted</p>`).addClass('corrupted').appendTo(elAttributes);
		el.addClass('corrupted');
	}

	// Handle (abyssal) jewels
	// We collect these and return them separately to be dealt with
	if (item.socketedItems) {
		item.socketedItems.forEach(function (socket) {
			if (socket.category.jewels) {
				abyssal.push(socket);
			}
		});
	}

	return {el, abyssal};
}
