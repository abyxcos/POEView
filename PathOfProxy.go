package main

import "net/http"
import "net/url"
import "html"
import "io/ioutil"
import "fmt"
import "log"

var site = "https://www.pathofexile.com"
var api_site = "http://api.pathofexile.com"

func main() {
	http.HandleFunc("/characters", getCharactersHandler)
	http.HandleFunc("/characters/get-items", getItemsHandler)
	http.HandleFunc("/characters/get-passive-skills", getPassivesHandler)
	http.HandleFunc("/leagues", getLeaguesHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

// character-window/get-characters/?accountName=%s
func getCharactersHandler(w http.ResponseWriter, r *http.Request) {
	path := "/character-window/get-characters"
	data,_ := url.QueryUnescape(r.URL.RawQuery)
	data = html.UnescapeString(data)

	query := fmt.Sprintf("%s%s?%s", site, path, data)

	fetchPage(w, query)
	fmt.Fprintf(w, "\n")
}

// character-window/get-items?accountName=%s&character=%s
func getItemsHandler(w http.ResponseWriter, r *http.Request) {
	path := "/character-window/get-items"
	data,_ := url.QueryUnescape(r.URL.RawQuery)
	data = html.UnescapeString(data)

	query := fmt.Sprintf("%s%s?%s", site, path, data)

	fetchPage(w, query)
	fmt.Fprintf(w, "\n")
}

// character-window/get-passive-skills?accountName=%s&character=%s
func getPassivesHandler(w http.ResponseWriter, r *http.Request) {
	path := "/character-window/get-passive-skills"
	data,_ := url.QueryUnescape(r.URL.RawQuery)
	data = html.UnescapeString(data)

	query := fmt.Sprintf("%s%s?%s", site, path, data)

	fetchPage(w, query)
	fmt.Fprintf(w, "\n")
}

// leagues?type=main&compact=1
func getLeaguesHandler(w http.ResponseWriter, r *http.Request) {
	path := "/leagues"
	query := fmt.Sprintf("%s%s?%s", api_site, path, r.URL.RawQuery)

	fetchPage(w, query)
	fmt.Fprintf(w, "\n")
}

func fetchPage(w http.ResponseWriter, url string) {
	// We want to enable CORS on all endpoints
	w.Header().Set("Access-Control-Allow-Origin", "*")

	resp, err := http.Get(url)
	if err != nil {
		fmt.Fprintf(w, "Got back error #%q\n", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	w.Write(body)
}
